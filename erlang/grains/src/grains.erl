-module(grains).

-export([square/1, total/0, test_version/0]).

square(_N) ->
  round(math:pow(2, _N - 1)).

total() ->
  round(math:pow(2, 64)) - 1.

test_version() -> 1.
