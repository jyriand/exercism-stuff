#!/usr/bin/env bash

set -o errexit
set -o nounset

main() {
    nr_of_squares=$1

    if [ $1 -le "0" ] || [ $1 -gt "64" ]
    then
        echo "Error: invalid input"
        exit 1
    fi


    sum_of_grains=1
    for ((i=2; i<=$1; i++))
    do
        sum_of_grains=$(bc<<<"$sum_of_grains * 2")
    done
    echo $sum_of_grains
}

if [ $1 == "total" ]
then
    total=0
    for ((i=1; i<=64; i++))
    do
        total=$total+$(main $i)
    done

else
    main "$1"
fi
