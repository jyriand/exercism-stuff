(defpackage #:leap
  (:use #:common-lisp)
  (:export #:leap-year-p))
(in-package #:leap)

(defun divides-evenly-p (number divisor)
  (= (mod number divisor) 0))

(defun leap-year-p (year)
  (and (divides-evenly-p year 4)
       (not (and (divides-evenly-p year 100)
                 (not (divides-evenly-p year 400))))))
