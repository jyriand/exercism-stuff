(in-package #:cl-user)
(defpackage #:space-age
  (:use #:common-lisp)
  (:export #:on-earth))

(in-package #:space-age)

(defparameter earth-oribital-period 31557600)
(defparameter mercury-oribital-period (* earth-oribital-period 0.2408467))
(defparameter mars-oribital-period (* earth-oribital-period 0.61519726))
(defparameter venus-oribital-period (* earth-oribital-period 1.8808158))
(defparameter jupiter-oribital-period (* earth-oribital-period 1.8808158))
(defparameter uranus-oribital-period (* earth-oribital-period 1.8808158))
(defparameter neptune-oribital-period (* earth-oribital-period 1.8808158))


(defun on-mercury (seconds))
(defparameter earth-oribital-period 31557600)
(defun on-earth (seconds) )

(defun on-mars (seconds))

(defun on-venus (seconds))
(defun on-jupiter (seconds))
(defun on-saturn (seconds))
(defun on-uranus (seconds))
(defun on-neptune (seconds))
