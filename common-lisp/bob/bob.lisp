(in-package #:cl-user)
(defpackage #:bob
  (:use #:cl)
  (:export #:response-for))
(in-package #:bob)

(defparameter *silence-chars* '(#\space #\tab #\newline #\page))

(defun yellingp (input)
  (flet ((alphabeticp (c)
           (and (char<= #\A c) (char>= #\z c)))
         (upcasep (c) (char= (char-upcase c) c)))
    (and
     (some #'alphabeticp input)
     (every #'upcasep input))))

(defun questionp (input)
  (char= (aref (reverse input) 0) #\?))

(defun silencep (input)
  (= (length input) 0))

(defun response-for (input)
  (when input
    (let ((input (string-trim *silence-chars* input)))
      (cond
        ((silencep input) "Fine. Be that way!")
        ((and (questionp input) (yellingp input))
         "Calm down, I know what I'm doing!")
        ((questionp input) "Sure.")
        ((yellingp input) "Whoa, chill out!")
        (t "Whatever.")))))
