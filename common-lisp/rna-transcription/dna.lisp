(in-package #:cl-user)
(defpackage #:dna
  (:use #:cl)
  (:export #:to-rna))
(in-package #:dna)

(defun replace-nucleotide (n)
  (case n
    (#\G #\C)
    (#\C #\G)
    (#\T #\A)
    (#\A #\U)
    (otherwise (error "Invalid nucleotide"))))

(defun transcribe (dna)
  (mapcar #'replace-nucleotide dna))

(defun to-rna (str)
  "Transcribe a string representing DNA nucleotides to RNA."
  (when (typep str 'string)
    (let ((dna-strand (coerce str 'list)))
      (coerce (transcribe dna-strand) 'string))))
