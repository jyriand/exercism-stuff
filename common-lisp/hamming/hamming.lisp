(defpackage #:hamming
  (:use #:cl)
  (:export #:distance))

(in-package #:hamming)

(defun string-to-list (a-string)
  (map 'list #'identity a-string))

(defun distance (dna1 dna2)
  "Number of positional differences in two equal length dna strands."
  (when (= (length dna1) (length dna2))
    (distance-iter (string-to-list dna1)
                   (string-to-list dna2)
                   0)))


(defun distance-iter (dna1 dna2 sum)
  (if (and dna1 dna2)
      (distance-iter (cdr dna1) (cdr dna2)
                     (if (equal (car dna1) (car dna2))
                         sum
                         (1+ sum)))
      sum))
