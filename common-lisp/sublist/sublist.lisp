(defpackage #:sublist
  (:use #:common-lisp)
  (:export #:sublist))

(in-package #:sublist)

(defun not-null (object)
  (not (null object)))

(defun sublist-recur (list1 list2)
  (when (and list1 list2)
    (or
     (every #'not-null (mapcar #'equal list1 list2))
     (sublist-recur list1 (cdr list2)))))

(defun sublistp (list1 list2)
  (when (< (length list1) (length list2))
    (or
     (not list1)
     (sublist-recur list1 list2))))

(defun sublist (list1 list2)
  "what is list1 of list2 (sublist, superlist, equal or unequal)"
  (cond
    ((equal list1 list2) "equal")
    ((sublistp list1 list2) "sublist")
    ((sublistp list2 list1) "superlist")
    (t "unequal")))
