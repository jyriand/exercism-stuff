(ql:quickload :cl-ppcre)

(defpackage #:acronym
  (:use #:cl)
  (:export #:acronym))
(in-package #:acronym)

(defun trim-delimiters (words)
  (when words
    (let ((delimiters '(#\, #\. #\: #\))))
      (mapcar (lambda (word) (string-trim delimiters word)) words))))

(defun acronym (name)
  (let ((trimmed-words (trim-delimiters (cl-ppcre:split "[ -]" name))))
    (reduce (lambda (result word)
              (concatenate 'string result
                           (string-capitalize (subseq word 0 1))))
            trimmed-words
            :initial-value "")))
